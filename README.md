# Installation instructions

I assume you have a Raspberry Pi, and have downloaded the latest Minecraft Pi Edition.


The easiest way to run the Minesweeper game is to just download and unzip the zip file provided on the [downloads](https://bitbucket.org/dwales/minesweeper-for-minecraft-pi-edition/downloads) page. You can then navigate to it, and run the script inside.  

If you really don't like the idea of having duplicate copies of the minecraft API on your computer, you can follow the slightly more complicated steps below:

When you downloaded Minecraft Pi Edition, there would have been a folder with the following structure: `minecraft/api/python/mcpi/<lots of files>`
To make this Minesweeper game run, without using the api folder I provide in the ZIP, you need to either copy my Python script to the `python` directory, and change a line in my Python script from `import minecraft.minecraft as minecraft` to `import mcpi.minecraft as minecraft`, (Note that the `minecraft` folder I have provided in the zip is an exact copy of the `mcpi` folder provided in `minecraft/api/python/mcpi/` However, I like to have a separate folder called `Programs` which contains the python api and all my programs, so I copied the `mcpi` folder and renamed it `minecraft`.)

Once that is sorted out, just open a Terminal window, navigate to the folder containing my script, and type `python minecraft-minesweeper.py`.
### NOTE: Minecraft must be open and in game, otherwise the script will not work! In game means that you have opened Minecraft, and have opened a world. You can press escape (or "o") in game to get your mouse back so you can start the script.


# Gameplay
Watch a [video](http://www.youtube.com/watch?v=aWL8iSKhXVg) of me playing it really slowly because I had to hold my iPod in one hand and control both the mouse and the keyboard with the other...
The general idea is fairly simple:

* A minesweeper board will be created at the coordinates (0,0,0)
* Right-clicking with a sword will clear a block (It doesn't work if you left click, or if you right click with anything else...)
* Placing a torch will flag a block
* When all the mines are flagged, you win.
* If you hit a mine, you lose.
* Different blocks represent different numbers:
    * Glass blocks have 0 mines touching them.
    * Wooden planks have 1 mine touching them.
    * Cobblestone  has 2 mines touching it.
    * Coal has 3 mines touching it.
    * Iron Ore has 4 mines touching it.
    * Gold Ore has 5 mines touching it.
    * Diamond Ore has 6 mines touching it.
    * Gold Blocks have 7 mines touching them.
    * Diamond Blocks have 8 mines touching them.
